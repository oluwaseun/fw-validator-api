"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var app = express_1.default();
app.set("PORT", process.env.PORT || 3000);
app.get("/", function (req, res) {
    res.status(200).send({
        message: "Hello Flutterwave, Let's do this",
    });
});
app.listen(app.get("PORT"), function () {
    return console.log("Server running on port " + app.get("PORT"));
});
