process.env.NODE_CONFIG_DIR = "./src/config";
process.env.NODE_ENV = "test";

module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  testPathIgnorePatterns: ["\\\\node_modules\\\\", "/config", "\\\\dist\\\\"],
};
