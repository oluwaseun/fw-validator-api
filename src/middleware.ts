import { NextFunction, Request, Response } from "express";

class HttpException extends Error {
    status: number;
    message: string;
    constructor(status: number, message: string) {
        super(message);
        this.status = status;
        this.message = message;
    }
}

export default HttpException;

export function errorMiddleware(
    err: HttpException,
    _req: Request,
    res: Response,
    _next: NextFunction
) {
    res.status(err.status || 500).json({
        message: err.message || "Unexpected Error",
		status: "error",
		data: null
    });
}
