import { isDefined } from "../utils";

const POSSIBLE_CONDITIONS = ["eq", "neq", "gt", "gte", "contains"] as const;
export type Condition = typeof POSSIBLE_CONDITIONS[number];

type RuleProps = {
    field: string;
    condition: Condition;
    condition_value: any;
};

type Result<T> = {
    value?: T;
    error?: string;
    isSuccess: boolean;
};

export class Rule {
    get field(): string {
        return this.props.field;
    }

    get condition(): Condition {
        return this.props.condition;
    }

    get condition_value(): any {
        return this.props.condition_value;
    }

    private constructor(private props: RuleProps) {}

    public static create(props: RuleProps): Result<Rule> {
        // rule must be defined
        if (!isDefined(props)) {
            return { isSuccess: false, error: "rule is required." };
        }
        // rule must be a valid json object
        if (!isDefined(props.field)) {
            return { isSuccess: false, error: "field is required." };
        }
        if (!isDefined(props.condition)) {
            return { isSuccess: false, error: "condition is required." };
        }
        if (!isDefined(props.condition_value)) {
            return { isSuccess: false, error: "condition_value is required." };
        }

        if (!POSSIBLE_CONDITIONS.includes(props.condition)) {
            return {
                isSuccess: false,
                error: `condition must be one of eq, neq, gt, gte, contains.`,
            };
        }

        return { value: new Rule(props), isSuccess: true };
    }
}
