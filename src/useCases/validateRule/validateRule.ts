import { getNestedData, isDefined } from "../../utils";
import { Condition, Rule } from "../../valueObjects/rule";
import { ValidateRuleDTO, ValidationRuleResponseDTO } from "./validateRuleDTO";

type ValidationResult = {
    message: string;
    isValid: boolean;
    field_value?: any;
};

export function validateRule(dto: ValidateRuleDTO): ValidationRuleResponseDTO {
    const { data, rule: rawRule } = dto;

    const result = validationHelper(data, rawRule);

    return {
        status: result.isValid ? "success" : "error",
        message: result.message,
        statusCode: result.isValid ? 200 : 400,
        data: !result.field_value
            ? null
            : {
                  validation: {
                      error: !result.isValid,
                      field: rawRule.field,
                      field_value: result.field_value,
                      condition: rawRule.condition,
                      condition_value: rawRule.condition_value,
                  },
              },
    };
}

export function validationHelper(data: any, rawRule: any): ValidationResult {
    const ruleOrError = Rule.create(rawRule);
    if (!ruleOrError.isSuccess || !ruleOrError.value) {
        return { message: ruleOrError.error as string, isValid: false };
    }
    if (!isDefined(data)) {
        return { message: "data is required.", isValid: false };
    }

    const rule = ruleOrError.value;
    // Check that data is in field
    const { value: field_value, lessThan2 } = getNestedData(data, rule.field);

    if (!lessThan2) {
        return {
            isValid: false,
            message: `field ${rule.field} is more than 2 nesting levels deep.`,
        };
    }
    if (!field_value) {
        return {
            isValid: false,
            message: `field ${rule.field} is missing from data.`,
        };
    }

    const isConditionMet = conditionValidator(
        field_value,
        rule.condition_value,
        rule.condition
    );

    if (isConditionMet) {
        return {
            message: `field ${rule.field} successfully validated.`,
            isValid: true,
            field_value,
        };
    } else {
        return {
            message: `field ${rule.field} failed validation.`,
            isValid: false,
            field_value,
        };
    }
}

function conditionValidator(
    fieldValue: string,
    condition_value: any,
    condition: Condition
) {
    switch (condition) {
        case "eq":
            return fieldValue === condition_value;
        case "neq":
            return fieldValue !== condition_value;
        case "gt":
            return fieldValue > condition_value;
        case "gte":
            return fieldValue >= condition_value;
        case "contains":
            return fieldValue.includes(condition_value);
        default:
            return false;
    }
}
