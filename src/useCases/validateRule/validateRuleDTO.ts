export interface ValidateRuleDTO {
  rule: {
    field: string;
    condition: "gte";
    condition_value: 30;
  };
  data: {
    name: "James Holden";
    crew: "Rocinante";
    age: 34;
    position: "Captain";
    missions: 45;
  };
}

export interface ValidationRuleResponseDTO {
  message: string;
  status: "error" | "success";
  data: Record<string, any> | null;
  statusCode: 200 | 400;
}

