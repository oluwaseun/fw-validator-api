export const isDefined = (data: any) => {
  return data !== null && data !== undefined;
};

export function getNestedData(
  data: any,
  path: string
): { value?: any; lessThan2: boolean } {
  const keys = path.split(".");

  if (keys.length > 2) {
    return { lessThan2: false };
  } else if (keys.length === 2) {
    if (!data[keys[0]]) return { lessThan2: true };
    return { value: data[keys[0]][keys[1]], lessThan2: true };
  }
  return { value: data[keys[0]], lessThan2: true };
}
