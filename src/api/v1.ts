import express from "express";
import { validateRule } from "../useCases/validateRule/validateRule";
import { ValidateRuleDTO } from "../useCases/validateRule/validateRuleDTO";

export const v1Router = express.Router();

v1Router.get("/", (_req, res) => {
    return res.status(200).send({
        message: "My Rule-Validation API",
        status: "success",
        data: {
            name: "Oluwaseun Akinwunmi",
            github: "@204070",
            email: "lere.akinwunmi@gmail.com",
            mobile: "08032538103",
            twitter: "@smai_lee",
        },
    });
});

v1Router.post("/validate-rule", (req, res) => {
    const dto = req.body as ValidateRuleDTO;
    try {
        const { statusCode, ...result } = validateRule(dto);
        return res.status(statusCode).json(result);
    } catch (err) {
        return res.status(500).json({
            message: "Unexpected Error",
            status: "error",
            data: null,
        });
    }
});
