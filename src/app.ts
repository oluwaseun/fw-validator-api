import express from "express";
import cors from "cors";
import morgan from "morgan";
import helmet from "helmet";
import { v1Router } from "./api/v1";
import { errorMiddleware } from "./middleware";

export const app = express();

app.set("PORT", process.env.PORT || 3000);

app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use(
    morgan(
        `:remote-addr - :remote-user [:date[iso]] ":method :url HTTP/:http-version" :status :res[content-length] - :response-time ms ":referrer" ":user-agent"`
    )
);

app.use("/", v1Router);
app.use(errorMiddleware);
app.use("*", (_req, res) => {
    res.status(404).json({ message: "URL Not Found", status: "error", data: null });
});
