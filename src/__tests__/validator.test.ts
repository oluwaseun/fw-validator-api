import request from "supertest";
import { app } from "../app";

const url = "/validate-rule";
describe("validator route POST '/validate-rule'", () => {
    it("returns a correct response for correct rule and data", async () => {
        const dto = {
            rule: {
                field: "missions",
                condition: "gte",
                condition_value: 30,
            },
            data: {
                name: "James Holden",
                crew: "Rocinante",
                age: 34,
                position: "Captain",
                missions: 45,
            },
        };

        const response = await request(app).post(url).send(dto);

        expect(response.status).toBe(200);
        expect(response.body.message).toEqual(
            "field missions successfully validated."
        );
        expect(response.body.status).toEqual("success");
        expect(response.body.data).toEqual({
            validation: {
                error: false,
                field: "missions",
                field_value: 45,
                condition: "gte",
                condition_value: 30,
            },
        });
    });

    describe("return failed validation", () => {
        test("When data is an object and condition is not met", async () => {
            const dto = {
                rule: {
                    field: "missions",
                    condition: "eq",
                    condition_value: 30,
                },
                data: {
                    name: "James Holden",
                    crew: "Rocinante",
                    age: 34,
                    position: "Captain",
                    missions: 45,
                },
            };

            const response = await request(app).post(url).send(dto);
            const validResponse = {
                message: "field missions failed validation.",
                status: "error",
                data: {
                    validation: {
                        error: true,
                        field: "missions",
                        field_value: 45,
                        condition: "eq",
                        condition_value: 30,
                    },
                },
            };
            expect(response.body).toEqual(validResponse);
        });

        test("when data is a string and condition is not met", async () => {
            const dto = {
                rule: {
                    field: "0",
                    condition: "eq",
                    condition_value: "a",
                },
                data: "damien-marley",
            };
            const response = await request(app).post(url).send(dto);
            const validResponse = {
                message: "field 0 failed validation.",
                status: "error",
                data: {
                    validation: {
                        error: true,
                        field: "0",
                        field_value: "d",
                        condition: "eq",
                        condition_value: "a",
                    },
                },
            };
            expect(response.body).toEqual(validResponse);
        });

        describe("when required field is not passed", async () => {
            test("when field is not in rule", async () => {
                const dto = {
                    rule: {
                        condition: "gte",
                        condition_value: 30,
                    },
                    data: 1,
                };
                const response = await request(app).post(url).send(dto);
                const validResponse = {
                    message: "field is required.",
                    status: "error",
                    data: null,
                };
                expect(response.body).toEqual(validResponse);
            });

            test("when condition in rule", async () => {
                const dto = {
                    rule: {
                        field: "missions",
                        condition_value: 30,
                    },
                    data: 1,
                };
                const response = await request(app).post(url).send(dto);
                const validResponse = {
                    message: "condition is required.",
                    status: "error",
                    data: null,
                };
                expect(response.body).toEqual(validResponse);
            });

            test("when condition is not one of eq, neq, gt, gte, contains", async () => {
                const dto = {
                    rule: {
                        field: "missions",
                        condition: "lmao",
                        condition_value: 30,
                    },
                    data: 1,
                };
                const response = await request(app).post(url).send(dto);
                const validResponse = {
                    message:
                        "condition must be one of eq, neq, gt, gte, contains.",
                    status: "error",
                    data: null,
                };
                expect(response.body).toEqual(validResponse);
            });

            test("when condition_value not in rule", async () => {
                const dto = {
                    rule: {
                        field: "missions",
                        condition: "lmao",
                    },
                    data: 1,
                };
                const response = await request(app).post(url).send(dto);
                const validResponse = {
                    message: "condition_value is required.",
                    status: "error",
                    data: null,
                };
                expect(response.body).toEqual(validResponse);
            });
        });

        describe("when field is missing from data", async () => {
            test("when array index not in array data", async () => {
                const dto = {
                    rule: {
                        field: "5",
                        condition: "contains",
                        condition_value: "rocinante",
                    },
                    data: ["The Nauvoo", "The Razorback", "The Roci", "Tycho"],
                };

                const response = await request(app).post(url).send(dto);
                const validResponse = {
                    message: "field 5 is missing from data.",
                    status: "error",
                    data: null,
                };
                expect(response.body).toEqual(validResponse);
            });

            test("when field is not in data", async () => {
                const dto = {
                    rule: {
                        field: "missions",
                        condition: "gte",
                        condition_value: 30,
                    },
                    data: {
                        name: "James Holden",
                        crew: "Rocinante",
                        age: 34,
                        position: "Captain",
                    },
                };

                const response = await request(app).post(url).send(dto);
                const validResponse = {
                    message: "field missions is missing from data.",
                    status: "error",
                    data: null,
                };
                expect(response.body).toEqual(validResponse);
            });

            test("when field nesting is greater than 2 levels deep", async () => {
                const dto = {
                    rule: {
                        field: "missions.a.b",
                        condition: "gte",
                        condition_value: 30,
                    },
                    data: {
                        name: "James Holden",
                        crew: "Rocinante",
                        age: 34,
                        position: "Captain",
                        missions: {
                            a: {
                                b: "c",
                            },
                        },
                    },
                };

                const response = await request(app).post(url).send(dto);
                const validResponse = {
                    message:
                        "field missions.a.b is more than 2 nesting levels deep.",
                    status: "error",
                    data: null,
                };
                expect(response.body).toEqual(validResponse);
            });
        });
    });
});
