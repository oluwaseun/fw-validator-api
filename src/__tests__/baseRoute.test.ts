import request from "supertest";
import { app } from "../app";

// TODO: Setup seed for test; change NODE_ENV before test.
describe("Check environment is TEST", () => {
  test("Environment should be TEST", () => {
    expect(process.env.NODE_ENV).toBe("test");
  });
});

describe("base route GET '/'", () => {
  test("returns a properly structure response with my personal data", async () => {
    const response = await request(app).get("/");
    const validResponse = {
      message: "My Rule-Validation API",
      status: "success",
      data: {
        name: "Oluwaseun Akinwunmi",
        github: "@204070",
        email: "lere.akinwunmi@gmail.com",
        mobile: "08032538103",
        twitter: "@smai_lee",
      },
    };
    expect(response.status).toBe(200);
    expect(response.body).toEqual(validResponse);
  });
});
